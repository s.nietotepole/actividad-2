package ito.poo;

import Clases.Composiciones;

import java.time.LocalDate;
import java.util.Scanner;

public class MyApp {
	
	static void run() {
		
		Composiciones c;
		c = new Composiciones("Viva el rock", 3.30, "Samuel Nieto", "Rock");
		
		c.setFechaRegistro(LocalDate.of(2021, 10, 3) );
		c.setFechaEstreno(LocalDate.of(2021, 10, 13));
		
		System.out.println(c);
		
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		run();
	}

}
